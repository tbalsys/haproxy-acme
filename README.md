# Build 

```
podman build . -t quay.io/tbalsys/haproxy-acme:2.4.8-3.0.1-r5
```

# Run

```
podman run \
  --name haproxy-acme \
  -e CRON_SCHEDULE='0 0 * * *' \
  -e ACME_HOME=/certs/acme
  -e HE_Username=email@example.com \
  -e HE_Password=secret \
  -v ./haproxy:/etc/haproxy \
  -v ./certs:/certs \
  quay.io/tbalsys/haproxy-acme:2.4.8-3.0.1-r5
```

Issue a new certificate:

```
podman exec haproxy-acme \
  acme.sh --issue \
  --home '$ACME_HOME' \
  --dns dns_he \
  -d example.com \
  -d *.example.com \
  --renew-hook "acme.sh --home '$ACME_HOME' --deploy -d example.com --deploy-hook haproxy"
```
