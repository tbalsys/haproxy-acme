FROM docker.io/alpine:edge

RUN apk --no-cache add acme.sh xxd haproxy msmtp \
  && ln -sf /usr/bin/msmtp /usr/sbin/sendmail \
  && curl -L -s https://github.com/just-containers/s6-overlay/releases/download/v2.1.0.2/s6-overlay-amd64.tar.gz | tar xvzf - -C /

ENV S6_BEHAVIOUR_IF_STAGE2_FAILS=2
ENV DEPLOY_HAPROXY_PEM_PATH=/certs/haproxy
ENV DEPLOY_HAPROXY_RELOAD="s6-svc -2 /var/run/s6/services/haproxy"
ENV ACME_HOME=/certs/acme
ENV CRON_SCHEDULE="0 2 * * *"

VOLUME /certs

COPY rootfs /

ENTRYPOINT [ "/init" ]
